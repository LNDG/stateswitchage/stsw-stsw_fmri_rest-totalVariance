% 171114 JQK: This script calculates the variance that was removed by
% applying FIX. Note that these are all still in the individual subject
% space.

% 171218 | added percentage variance reduction
% 180220 | adpated for STSWD YA

% N = 43 (1126 has no Rest data)
IDs = {'1117';'1118';'1120';'1124';'1125';'1131';'1132';'1135';'1136';'1151';'1160';'1164';'1167';'1169';'1172';'1173';'1178';'1182';'1214';'1215';'1216';'1219';'1223';'1227';'1228';'1233';'1234';'1237';'1239';'1240';'1243';'1245';'1247';'1250';'1252';'1257';'1261';'1265';'1266';'1268';'1270';'1276';'1281'};

for indID = 1:numel(IDs)
    
    ID = IDs{indID}; disp(ID);
    pn.subjFolder = ['/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/preproc/B_data/D_preproc/',ID,'/preproc/rest/'];
    pn.postFix = [pn.subjFolder, ID, '_rest_feat_detrended_bandpassed_manualdenoise_MNI3mm.nii.gz'];
    pn.outFolder = '/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/D_totalVariance/B_data/';
    
    addpath(genpath('/Volumes/LNDG/Projects/StateSwitch/dynamic/data/mri/rest/analyses/D_totalVariance/D_tools/'));

    postFix = load_untouch_nii(pn.postFix);

    % compare voxel-wise variability between the two images
    varPostFix = std(postFix.img,[],4);
    varPostFix_scaled = varPostFix./nanmean(nanmean(nanmean(varPostFix)));
    
    MergedMatPost(indID,:,:,:) = varPostFix_scaled;
end % ID loop

postFix.img = squeeze(nanmean(MergedMatPost,1));
save_untouch_nii(postFix, [pn.outFolder, 'allSubs_mean_STDscaled.nii.gz']);

postFix.img = squeeze(nanstd(MergedMatPost,[],1));
save_untouch_nii(postFix, [pn.outFolder, 'allSubs_var_STDscaled.nii.gz']);
    