## D_totalVariance

Curator: JQK

Purpose: Create variance images from preprocessed data.

Scripts

A_calculateVarFIX_rel

- N = 43 YAs (1126 no rest)
- Load v1 preprocessed images, calculate individual SD BOLD relative to mean BOLD in image
- Average (allSubs_mean_STDscaled) and STS across subjects (allSubs_var_STDscaled)
